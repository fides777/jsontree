@extends('layouts.app')

@section('content')

@if(count($errors)>0)
    <ul>
    @foreach($errors->all() as $error)
        <li class="alert alert-danger">
            {{$error}}
        </li>
    @endforeach
    </ul>
@endif

    <div class="tree well">
     <ul>
         @foreach($categories as $category)
            <li>
                <span><i class="icon-leaf icon-plus-sign"></i> {{ $category->name }}</span> <a href=""></a>

                @if(count($category->childs))
                    @include('tree/childs',['childs' => $category->childs])
                @endif
            </li>
         @endforeach

     </ul>
</div>

<div class="col-md-6">

    <form method="POST" action="/"  enctype="multipart/form-data">
        <div class="form-group">
            @csrf
            <label for="FormControlFile">Json file input</label>
            <input type="file" name = "file" class="form-control-file" id="FormControlFile">
            <button type="submit">Submit</button>
        </div>
    </form>
</div>


@endsection