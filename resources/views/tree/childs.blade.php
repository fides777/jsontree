<ul>
    @foreach($childs as $child)
            <li style="display: none">
                <span><i class="icon-leaf icon-plus-sign"></i> {{ $child->name }}</span>
            @if(count($child->childs))
                @include('tree/childs',['childs' => $child->childs])
            @endif
            </li>

    @endforeach
</ul>