<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tree extends Model
{
    protected $table = 'trees';

    protected $fillable = [
        'parent_id',
        'name'
    ];

    public function childs() {

        return $this->hasMany('App\Tree','parent_id','id') ;

    }
}
