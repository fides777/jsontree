<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTreeFileRequest;
use App\Tree;
use Illuminate\Http\Request;
use Validator;

class TreeController extends Controller
{
    public function index()
    {

       $tree = Tree::get();


        $categories = Tree::where('parent_id', '=', 0)->get();
        $allCategories = Tree::pluck('name','id')->all();

        return view('tree/index')->with(['trees' => $tree, 'categories' => $categories, 'childs' => $allCategories]);
    }

    public function storeFile(StoreTreeFileRequest $request)
    {

        $request->file('file');
        $string = file_get_contents($request->file('file'));

        $json_a =json_decode($string, true);

        $data = ['data' => $json_a];

        $validator = Validator::make($data,
            [
                'data.*.id' => 'required|integer|max:100',
                'data.*.name' => 'required|string|max:100',
                'data.*.parent_id' => 'required|max:100',
            ]);

        if($validator->fails()) {
            return redirect('/')
                ->withErrors($validator);
        }
        else {
            foreach ($json_a as $data) {
                Tree::create($data);
            }
        }

        return redirect('/');
    }


}
